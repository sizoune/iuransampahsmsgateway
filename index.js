require("dotenv").config();
const express = require("express");
const { json, urlencoded } = express;

const app = express();

app.use(json());

app.use(
  urlencoded({
    extended: true,
  })
);
const port = 3000;

const { Vonage } = require("@vonage/server-sdk");

const vonage = new Vonage({
  apiKey: process.env.API_KEY,
  apiSecret: process.env.API_SECRET,
});

app.get("/", (req, res) => {
  res.send("Hello World!");
});

app.post("/sendsms", async (req, res) => {
  const { tujuan: to, isiPesan: text } = req.body;
  const from = "Iuran Sampah";
  if (!to) {
    res.statusCode = 422;
    res.send({
      status: "Gagal",
      messsage: "tujuan tidak didefiniskan pada body!",
    });
  }

  if (!text) {
    res.statusCode = 422;
    res.send({
      status: "Gagal",
      messsage: "isiPesan tidak didefiniskan pada body!",
    });
  }

  try {
    await vonage.sms.send({ to, from, text });
    res.statusCode = 200;
    res.send({
      status: "Sukses",
      messsage: "Sms berhasil terkirim",
    });
  } catch (error) {
    console.log(error);
    res.statusCode = 500;
    res.send({
      status: "Gagal",
      messsage: error.messsage || "Terjadi kesalahan pada server!",
    });
  }
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
